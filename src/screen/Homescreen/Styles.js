import {StyleSheet} from 'react-native';
export const Styles = StyleSheet.create({


    main_view:{
        flex:1,
        backgroundColor:'#2C3E50',
        // borderWidth:1,
        // borderColor:'red'
    },
    text: {
        margin:10,
        fontSize:25,
        fontWeight:'bold',
        color:'#2ECC71'
    },
    body_view:{
        flex:1,
        // borderWidth:1,
        // borderColor:'red'
    },
    text_style:{
        margin:10,
        color:'#FFFFFF'
    },
    textPopular:{
        margin:5,
        flexDirection:'row'
    },
    Image1:{
        // margin:5,
        height:230,
        width:375,
    },
    continue_watching_style:{
        fontSize:14,
        fontWeight:'bold',
        color:'#FFFFFF',
        // height:21,
        // width:141,
    },
    Image2:{
        margin:2,
        height:140,
        width:178,
        // borderWidth:1,
        // borderColor:'red'
        borderRadius:10
    },
    Image3:{
        margin:3,
        height:157,
        width:101,
    },
    Suggested_text:{
        fontSize:14,
        fontWeight:'bold',
        color:'#FFFFFF',
        height:21,
        width:131,
        marginTop:10
    },
    movie_text:{
        margin:3,
        fontSize:14,
        fontWeight:'bold',
        color:'#FFFFFF',

    },
    Image5:{
        height:64,
        width:64,
        borderRadius:64/2,
    }

})